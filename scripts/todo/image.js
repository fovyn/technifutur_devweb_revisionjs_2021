class ImageService {
    /**
     * 
     * @param {File} file 
     */
    static toBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);

            reader.addEventListener("load", function(e) {
                console.log("READER_LOAD");
                resolve(this.result);
            });

            reader.addEventListener("error", function(error) {
                reject(error);
            })
        })
    }

    static resize(src, dimension) {
        return new Promise((resolve, reject) => {
            const img = new Image();
            img.src = src;

            const canvas = document.createElement("canvas");
            canvas.width = dimension.width;
            canvas.height = dimension.height;

            const context = canvas.getContext("2d");

            img.addEventListener("load", function(){
                context.scale(dimension.width/this.width, dimension.height/this.height);
                context.drawImage(this, 0, 0);
                resolve(canvas.toDataURL());
            })
        });
    }
}
class ImageField {
    /** @type {HTMLInputElement} */
    #input;
    /** @type {File} */
    #file;
    /** @type {HTMLImageElement} */
    #img;
    /** @type {{width: number, height: number}} */
    #dimension;
    #base64;

    /**
     * 
     * @param {string} selector 
     * @param {{width: number, height: number}} dimension 
     */
    constructor(selector, dimension = {width: 100, height: 100}) {
        const container = document.querySelector(selector);
        this.#input = container.querySelector("input");
        this.#img = container.querySelector("img");

        this.#dimension = dimension;

        this.#input.addEventListener("change", e => {
            this.#change(e);
        });
    }

    get base64() { return this.#base64; }

    #change = function(e) {
        this.#file = this.#input.files[0];
        ImageService.toBase64(this.#file).then(url => {
            ImageService.resize(url, this.#dimension).then(urlResize => {
                this.#img.src = urlResize;
                this.#base64 = urlResize;
            });
        });
    }
}