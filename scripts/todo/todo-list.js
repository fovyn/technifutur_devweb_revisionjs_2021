class BehaviorSubject {
    #value;
    #observers;

    constructor(value) {
        this.#value = value;
        this.#observers = [];
    }

    get value() { return this.#value; }
    next(newValue) {
        this.#value = newValue;
        this.#next();
    }

    subscribe(action) {
        this.#observers.push(action);
        this.#next();
    }

    #next = function() {
        for(let observer of this.#observers) {
            observer(this.#value);
        }
    }
}

class TodoList {
    /** @type {BehaviorSubject<Array<Todo>>} */
    #todoes;

    /**
     * 
     * @param {Array<Todo>} todoes 
     */
    constructor(todoes = null) {
        this.#todoes = new BehaviorSubject(todoes || []);
    }

    /**
     * 
     * @param {Todo} todo 
     */
    add(todo) {
        this.#todoes.next([...this.#todoes.value, todo]);
    }

    subscribe(action) {
        this.#todoes.subscribe(action);
    }
}