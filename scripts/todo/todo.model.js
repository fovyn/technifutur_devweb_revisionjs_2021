class Todo {
    #title;
    #description;
    #dueDate;
    #image;
    #state;

    constructor(obj) {
        this.title = obj && obj.title || null;
        this.description = obj && obj.description || null;
        this.dueDate = obj && obj.dueDate || moment();
        this.state = obj && obj.state || "à faire";
        this.image = obj && obj.image || null;
    }

    get title() { return this.#title; }
    set title(v) { this.#title = v; }

    get description() { return this.#description; }
    set description(v) { this.#description = v; }

    get dueDate() { return this.#dueDate; }
    set dueDate(v) { this.#dueDate = v; }

    get image() { return this.#image; }
    set image(v) { this.#image = v; }

    get state() { return this.#state; }
    /**
     * @param {"urgent" | "fait" | "à faire"} v
     */
    set state(v) { this.#state = v; }
}